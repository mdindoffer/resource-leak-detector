package eu.dindoffer.resourceleak.example.core;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class LeakDetector<T> extends Thread {

    private final ReferenceQueue<T> queue = new ReferenceQueue<>();
    private final Set<UUID> usedIDs = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private final Map<T, UUID> objectIdentifiers = new WeakHashMap<>();
    private final ConcurrentMap<UUID, LeakInfo> trackedResources = new ConcurrentHashMap<>();
    private boolean continueTracking;

    public LeakDetector() {
        super("Resource Leak Detector");
        this.setDaemon(true);
        this.continueTracking = true;
    }

    public synchronized void acquired(T resource) throws IllegalArgumentException {
        if (objectIdentifiers.containsKey(resource)) {
            throw new IllegalArgumentException("Resource already being tracked");
        }
        UUID id = generateUniqueID();
        objectIdentifiers.put(resource, id);
        LeakInfo leakInfo = new LeakInfo(resource, id);
        trackedResources.put(id, leakInfo);
    }

    public synchronized void released(T resource) throws IllegalArgumentException {
        UUID id = objectIdentifiers.get(resource);
        if (id == null) {
            throw new IllegalArgumentException("Tried to release not tracked resource");
        }
        usedIDs.remove(id);
        trackedResources.remove(id);
    }

    @Override
    public void run() {
        try {
            while (continueTracking) {
                LeakInfo leakInfo = (LeakInfo) queue.remove();
                usedIDs.remove(leakInfo.id);
                if (trackedResources.remove(leakInfo.id) != null) {
                    leaked(leakInfo);
                }
            }
        } catch (InterruptedException x) {
            // Exit
        }
    }

    private UUID generateUniqueID() {
        UUID idCandidate;
        do {
            idCandidate = UUID.randomUUID();
        } while (usedIDs.contains(idCandidate));
        usedIDs.add(idCandidate);
        return idCandidate;
    }

    private void leaked(LeakInfo leakInfo) {
        System.err.println("Resource leaked: " + leakInfo.description);
        System.err.println("Acquisition stack trace:");
        leakInfo.stackFrames.printStackTrace(System.err);
    }

    public class LeakInfo extends PhantomReference<T> {

        private final UUID id;
        private final String description;
        private final Throwable stackFrames;

        private LeakInfo(T referent, UUID id) {
            super(referent, queue);
            this.id = id;
            this.description = referent.toString();
            this.stackFrames = new Throwable();
        }

        public String getResourceDescription() {
            return description;
        }

        public Throwable getStackFrames() {
            return stackFrames;
        }

        @Override
        public String toString() {
            return description;
        }
    }
}
