package eu.dindoffer.resourceleak.example.core;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class SomeResource {

    private final int number;
    private final LeakDetector<SomeResource> detector;

    public SomeResource(LeakDetector<SomeResource> detector, int number) {
        this.detector = detector;
        this.number = number;
    }

    public void close() {
        detector.released(this);
    }

    @Override
    public String toString() {
        return "Some resource n. " + number;
    }
}
