package eu.dindoffer.resourceleak.example.core;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class ResourceProvider {
    
    private int resourceCounter;
    private final LeakDetector<SomeResource> detector;
    
    public ResourceProvider() {
        this.resourceCounter = 0;
        this.detector = new LeakDetector<>();
        detector.start();
    }
    public SomeResource acquireResource() {
        SomeResource someResource = new SomeResource(detector, ++resourceCounter);
        detector.acquired(someResource);
        return someResource;
    }
}
