package eu.dindoffer.resourceleak.example.gui;

import eu.dindoffer.resourceleak.example.core.ResourceProvider;
import eu.dindoffer.resourceleak.example.core.SomeResource;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class MainWindowController {

    @FXML
    private VBox myRoot;

    private final ResourceProvider resProvider = new ResourceProvider();
    @FXML
    private Button btnAcquire;
    @FXML
    private ListView<SomeResource> lviewResources;
    @FXML
    private Button btnNullify;
    @FXML
    private Button btnClose;

    @FXML
    private void onbtnAcquire(ActionEvent event) {
        SomeResource acquiredResource = resProvider.acquireResource();
        lviewResources.getItems().add(acquiredResource);
    }

    @FXML
    private void onbtnNullify(ActionEvent event) {
        SomeResource selectedResource = lviewResources.getSelectionModel().getSelectedItem();
        lviewResources.getItems().remove(selectedResource);
    }

    @FXML
    private void onbtnClose(ActionEvent event) {
        SomeResource selectedResource = lviewResources.getSelectionModel().getSelectedItem();
        if (selectedResource != null) {
            selectedResource.close();
        }
        lviewResources.getItems().remove(selectedResource);
    }
}
